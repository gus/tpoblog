<!doctype html>
<html>
<head>
    <title>Making Tor Browser Updates Stable and Reliable with Fastly | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Making Tor Browser Updates Stable and Reliable with Fastly | Tor Project">
    <meta property="og:description" content="Tor Browser is well-known for its tracking protection and fingerprinting resistance. We have...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/making-tor-browser-updates-stable-and-reliable-fastly/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Making Tor Browser Updates Stable and Reliable with Fastly
      </h1>
    <p class="meta">by gk | December 14, 2017</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><img alt="tor-fastly" src="/static/images/blog/inline-images/tor-fastly.png" /><br />
Tor Browser is well-known for its <a href="https://www.torproject.org/projects/torbrowser/design/#identifier-linkability" rel="nofollow">tracking protection</a> and <a href="https://www.torproject.org/projects/torbrowser/design/#fingerprinting-linkability" rel="nofollow">fingerprinting resistance</a>. We have spent a lot of time and energy as well to make the browser bit-by-bit reproducible in order to <a href="https://blog.torproject.org/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">defend against compromises of our build machines</a>. It is worth mentioning that this includes our update files, too, which are generated during the build process. But having reproducibly built update files, which are properly signed, is only one half of the setup we need: without being able to make them available in a timely and reliable manner our users are not as well-protected as they should be.</p>

<p>Back in 2014 when we introduced the browser updater with Tor Browser 4.0, we hosted the update files ourselves. But it soon became obvious that it would be a challenge to scale our infrastructure to keep up with the user demand and ever growing update sizes (just compare the 50 MB we had on Linux for full updates in 2014 to the 85 MB we have today). Gladly, <a href="https://www.fastly.com/open-source" rel="nofollow">Fastly</a> stepped up in 2016 to guarantee smooth updates for Tor Browser users. After some experiments, updates were provided over Fastly's infrastructure from June 2016 on. On average roughly 2.1TB/day has been transferred since then, with spikes of over 14TB/day. <strong>Thus, a big thank you, Fastly, for hosting our Tor Browser update files for the past 18 months!</strong></p>

<p><img src="https://extra.torproject.org/blog/2017-12-14-tor-fastly/fastly-18-months_final.png" width="530" /></p>

<p>We don't know exactly how many Tor Browser users those transferred 2.1TB/day represent, but we started to collect <a href="https://blog.torproject.org/tor-browser-numbers" rel="nofollow">download metrics a while back</a> in order to at least observe trends in user update behavior. If we look at the past 18 months (see image above), we can see that the amount of daily update pings, which check whether an update is available, continually rose up to 2,000,000 in February 2017 and has stayed more or less constant at that level (apart from the outlier between February 2017 and April 2017 which gets investigated in <a href="https://trac.torproject.org/projects/tor/ticket/22346" rel="nofollow">bug 22346</a>). Update requests on the other hand range from 600,000 to 1,200,000 on the day a new Tor Browser stable version gets released (with a sharp drop and a long tail afterwards) which shows the importance of a reliable and robust update infrastructure.</p>

<p>We'd like to emphasize that we've only been using Fastly for Tor Browser updates, which are fetched anonymously over Tor. One reason to focus just on updates is because updates produce the highest bandwidth spikes, which is where Fastly is helpful most to us. Moreover, Fastly <a href="https://docs.fastly.com/guides/compliance/security-program#customer-and-end-user-data-management" rel="nofollow">does not store customer request logs</a>, so they do not store logs for downloads of Tor Browser updates. This commitment is important as it shows a clear stance towards user privacy which is especially valuable in the update case: an attacker can't show up after the fact and learn update requests and behavior of particular users. This is a good first line of defense which is worth having in a <a href="https://trac.torproject.org/projects/tor/ticket/20928" rel="nofollow">non-update context</a> as well. But Tor Browser takes this a step further by updating its users solely over Tor. Even if there were logs available or someone were able to compromise Fastly's servers, it would not be possible to target a particular Tor Browser user with a malicious update or learn about their update behavior as all users show up as coming from the Tor network on Fastly's systems. This is not only relevant for Tor Browser but is in general an important feature as outlined <a href="https://blog.torproject.org/tor-heart-apt-transport-tor-and-debian-onions" rel="nofollow"> in an earlier blog post</a>.</p>

<p>Thanks to <a href="https://www.fastly.com" rel="nofollow">Fastly</a> we have found a way to solve the stability and reliability issues in our update infrastructure while still protecting our users. However, bandwidth load and update sizes will continue to rise, not only because we will hopefully attract even more users in the future, but also because we deliver Tor Browser to more and more platforms (see for instance the recently started <a href="https://blog.torproject.org/tor-browser-75a8-released" rel="nofollow">support for 64bit Windows systems</a>). This will likely introduce new scalability challenges given our privacy and security constraints and the need for reliable updates. But we are optimistic to solve them with our friends at Fastly should they arise.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/applications">
          applications
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-273046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273046" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273046" class="permalink" rel="bookmark">Finally this blog post about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Finally this blog post about fastly that arma was talking about for quiet some months ... x)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273047"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273047" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Matthew Prince suxx (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273047">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273047" class="permalink" rel="bookmark">[sarcasm] Why didn&#039;t you ask…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[sarcasm] Why didn't you ask your friends over at Cloudflare and Google reCaptcha? I'm sure they would've helped you scale far more reliably than Fastly by making you loose all of your users. [/sarcasm]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273047" class="permalink" rel="bookmark">[sarcasm] Why didn&#039;t you ask…</a> by <span>Matthew Prince suxx (not verified)</span></p>
    <a href="#comment-273117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273117" class="permalink" rel="bookmark">lol u must read this https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>lol u must read this <a href="https://trac.torproject.org/projects/tor/ticket/24351" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/24351</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273432"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273432" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PhukkGoogle (not verified)</span> said:</p>
      <p class="date-time">January 16, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273047" class="permalink" rel="bookmark">[sarcasm] Why didn&#039;t you ask…</a> by <span>Matthew Prince suxx (not verified)</span></p>
    <a href="#comment-273432">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273432" class="permalink" rel="bookmark">:-D :-D :-D Comment of the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>:-D :-D :-D Comment of the year award.<br />
Sick of correctly completing ReCaptchas to provide my unpaid labour to train Google's AI that will put me out of a job... only to not even get the respect of a functioning 'Submit' button when using Orfox. This is YEARS that this has been a bug. Google are actively harassing TOR users, in the most passive-aggressive manner, plausible-deniability built-in, of course.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273730"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273730" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273432" class="permalink" rel="bookmark">:-D :-D :-D Comment of the…</a> by <span>PhukkGoogle (not verified)</span></p>
    <a href="#comment-273730">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273730" class="permalink" rel="bookmark">Dude they(OP &amp; punks)…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dude they(OP &amp; punks) already provided a solution. <a href="https://addons.mozilla.org/en-US/firefox/addon/block-cloudflare-mitm-attack/" rel="nofollow">Try their add-on</a>. It automatically redirect to non-cloudflare webpage instead of solving a puzzle.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273048"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273048" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273048">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273048" class="permalink" rel="bookmark">I wonder how you can…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wonder how you can reliably scale up by using onion services to distribute updates, I know that update checks are planned, but do you have any idea on how to scale up the updates themselves using onion services?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273085" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 15, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273048" class="permalink" rel="bookmark">I wonder how you can…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273085" class="permalink" rel="bookmark">We have not thought about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have not thought about how to do that yet. Proposals are welcome. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-273112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273112" class="permalink" rel="bookmark">@ gk:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ gk:</p>
<p>Maybe discuss with Tails Project how Tor Project (or Debian Project?) can help Tails provide iso images for each new edition (i.e. by providing a repository, not via torrents)?</p>
<p>Many thanks for the onion mirrors for Debian!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273123" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>yes (not verified)</span> said:</p>
      <p class="date-time">December 17, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-273123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273123" class="permalink" rel="bookmark">not related to that, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>not related to that, but what about choosing randomly what node can become an entry node over a period of time<br />
that can drastically reduce the attack window for us law enforcement</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273127" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 18, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273123" class="permalink" rel="bookmark">not related to that, but…</a> by <span>yes (not verified)</span></p>
    <a href="#comment-273127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273127" class="permalink" rel="bookmark">I don&#039;t think that&#039;s a good…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think that's a good idea. For why the guard nodes are pinned for a while see: <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/CommunityTeam/Support#WhyisthefirstIPaddressinmyrelaycircuitalwaysthesame" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/teams/CommunityTeam/S…</a> with further links to blog post and paper.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-273055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273055" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273055" class="permalink" rel="bookmark">Why not use dist.tpo&#039;s onion…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why not use dist.tpo's onion address?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273084"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273084" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 15, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273055" class="permalink" rel="bookmark">Why not use dist.tpo&#039;s onion…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273084">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273084" class="permalink" rel="bookmark">Because that does not scale…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Because that does not scale right now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273058"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273058" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273058">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273058" class="permalink" rel="bookmark">I have a cybersecurity…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a cybersecurity question about Fastly and TB updates which requires a bit of background.</p>
<p>There has just occurred another highly suspicious BGP incident in which traffic from high-value sites was (apparently with malicious intent) rerouted through this mysterious RU ISP:</p>
<p>AS539523<br />
dv-hyperlink.ru<br />
Vasilyev Ivan Ivanovich<br />
8 Pionerskaya st., office 10<br />
Nekrasovka, Khabarovsk region Russia</p>
<p>The affected traffic included traffic to/from Google, Facebook, Apple, Microsoft, and the illicit BGP changes were picked up by some major US backbone providers including Hurricane Electric in the US and NORDUnet (which has hosted several fast Tor nodes).</p>
<p>&gt; Thus, a big thank you, Fastly, for hosting our Tor Browser update files for the past 18 months!</p>
<p>I think that traffic routed through Fastly would likely be trunked over Hurricane Electric.  So a malicious BGP announcement picked up by Hurricane could directly affect TB updates.  And according to 2015 Blackhat presentation, a carefully orchestrated BGP hijack could allow an attacker to issue themselves a valid TLS certificate for high value targets such as fastly.com:</p>
<p><a href="http://www.securityweek.com/should-you-be-worried-about-bgp-hijacking-your-https" rel="nofollow">http://www.securityweek.com/should-you-be-worried-about-bgp-hijacking-y…</a><br />
Should You Be Worried About BGP Hijacking your HTTPS?<br />
David Holmes<br />
9 Sep 2015</p>
<p>&gt; A BGP route monitoring firm, Qrator, released a paper at Blackhat 2015 titled “Breaking HTTPS with BGP Hijacking.” I’ll say more about this paper in a little bit, but let’s set up the basics of BGP first... According to the Qrator white paper, with a well-timed BGP hijack, an attacker could issue themselves a real TLS certificate from a real certificate authority. The attacker would advertise that they owned the target’s domain, and request a certificate for that domain from a certificate authority.</p>
<p>Question: if high value websites (such as a website offering legitimate TB updates) used onion sites instead of vanilla https, could this kind of attack be prevented?</p>
<p>If this is a stupid question based upon some misunderstanding of BGP, please explain.</p>
<p>[snip] [I cut the huge background material to make your comment more readable for all users, G.K.]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273086"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273086" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 15, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273058" class="permalink" rel="bookmark">I have a cybersecurity…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273086">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273086" class="permalink" rel="bookmark">For AS539523 please read…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For AS539523 please read AS39523.</p>
<p>Thanks for posting (some of) my question.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273070"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273070" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273070">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273070" class="permalink" rel="bookmark">For really improving the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For really improving the signature testing on TBB it would be really nice integrate the torproject-signature in TAILS.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273087" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 15, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273070" class="permalink" rel="bookmark">For really improving the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273087" class="permalink" rel="bookmark">Sounds like a good idea to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like a good idea to me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273070" class="permalink" rel="bookmark">For really improving the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273111" class="permalink" rel="bookmark">Actually, it would be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, it would be convenient (and potentially reassuring) if Tails and Tor both provided up to date public keys for each other, in case for some reason one offical source is suddenly unavailable to someone.</p>
<p>Obviously, maintaining up to date (and very carefully verified!) copies of public keys of other  projects could be a bit tricky, but it might be worth discussing with Tails Project and maybe some others such as Debian and Mozilla.  </p>
<p>Points to consider include:</p>
<p>1. By providing copies of keys from other projects, Tails Project would essentially be putting their credibility on the line by saying that these are the real TBB signing keys, and vice versa, so its essential not to make mistakes or to allow the bad guys to maliciously alter anything.</p>
<p>2. Debian Project includes in the Debian repos some packages which hold public keys for some allied projects like Mozilla developers, so this would not be unprecedented.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273078"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273078" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Spoonful (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273078">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273078" class="permalink" rel="bookmark">Hi. I was wondering if I can…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi. I was wondering if I can have access please.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tga (not verified)</span> said:</p>
      <p class="date-time">December 19, 2017</p>
    </div>
    <a href="#comment-273152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273152" class="permalink" rel="bookmark">How onerous would it be to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How onerous would it be to have exit relays serve updates? Clearly the bandwidth isn't an issue, since the updates are already going through them. The only issue I could see (aside from having to implement it) would be storage, but do people run exit relays on systems without 85 MB to spare?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273179" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2017</p>
    </div>
    <a href="#comment-273179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273179" class="permalink" rel="bookmark">Why have not you used IPFS…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why have not you used IPFS instead? It's more decentralised. But Fastly is a CDN - a global active adversary.  Why have you accepted this trojan horse-like gift?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273324"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273324" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tiny nailer (not verified)</span> said:</p>
      <p class="date-time">January 06, 2018</p>
    </div>
    <a href="#comment-273324">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273324" class="permalink" rel="bookmark">Fastly , found my other non…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fastly , found my other non-tor- browsers and installed a link which<br />
went back to them,  then disappeared, I used tcpview.exe and saw this many time and tried to fix<br />
but ended up blocking Fastly servers on my Cisco router, so I have real reservations<br />
that they are helping, tell me they have no moles?????</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273434"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273434" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 16, 2018</p>
    </div>
    <a href="#comment-273434">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273434" class="permalink" rel="bookmark">A handful of CDNs is like a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A handful of CDNs is like a handful of physical TV distribution networks: playing into the hands of Totalitarians by providing only a few distinct points of pressure in the system.<br />
Now, for example in the UK, we wonder at how supposedly 'independent' TV channels can be pushing complementary propaganda against 'Scrounging' (leeching) Disabled people on welfare.<br />
Which didn't happen before the current (2010 onwards) Government agenda to "compete with the Chinese" (whose human rights and welfare records speak for themselves).<br />
The truth is NOT commonly known and talked-about in the minds of the people of the UK. The propaganda IS. Just one example (and the policy that results from this control and manipulation of the media was condemned at the UN).</p>
<p><a href="https://www.independent.co.uk/news/uk/home-news/government-disabled-people-un-convention-treatment-breach-a7900176.html" rel="nofollow">https://www.independent.co.uk/news/uk/home-news/government-disabled-peo…</a></p>
<p>So, using and/or trusting CDNs is arguably helping provide the infrastructure to help our common enemies. Only decentralised control can avoid this. Or, let's have the NSA offer-over some of their vast botnet capacity in the interests of 'freedom', eh? ;-p</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
