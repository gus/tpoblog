<!doctype html>
<html>
<head>
    <title>New low cost traffic analysis attacks and mitigations | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="New low cost traffic analysis attacks and mitigations | Tor Project">
    <meta property="og:description" content="Recently, Tobias Pulls and Rasmus Dahlberg published a paper entitled Website Fingerprinting with Website Oracles.">
    <meta property="og:image" content="https://blog.torproject.org/new-low-cost-traffic-analysis-attacks-mitigations/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/new-low-cost-traffic-analysis-attacks-mitigations/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        New low cost traffic analysis attacks and mitigations
      </h1>
    <p class="meta">by mikeperry | December 19, 2019</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Recently, Tobias Pulls and Rasmus Dahlberg published a paper entitled <a href="https://petsymposium.org/2020/files/papers/issue1/popets-2020-0013.pdf" rel="nofollow">Website Fingerprinting with Website Oracles</a>.</p>

<p>"Website fingerprinting" is a category of attack where an adversary observes a user's encrypted data traffic, and uses traffic timing and quantity to guess what website that user is visiting. In this attack, the adversary has a database of web pages, and regularly downloads all of them in order to record their traffic timing and quantity characteristics, for comparison against encrypted traffic, to find potential target matches.</p>

<p>Practical website traffic fingerprinting attacks against the live Tor network have been limited by the sheer quantity and variety of all kinds (and combinations) of traffic that the Tor network carries. The paper reviews some of these practical difficulties in sections 2.4 and 7.3.</p>

<p>However, if specific types of traffic can be isolated, such as through <a href="https://blog.torproject.org/technical-summary-usenix-fingerprinting-paper" rel="nofollow">onion service circuit setup fingerprinting</a>, the attack seems more practical. This is why we recently <a href="https://gitweb.torproject.org/torspec.git/tree/padding-spec.txt#n282" rel="nofollow">deployed cover traffic</a> to obscure client side onion service circuit setup.</p>

<p>To address the problem of practicality against the entire Internet, this paper uses various kinds of public Internet infrastructure as side channels to narrow the set of websites and website visit times that an adversary has to consider. This allows the attacker to add confidence to their classifier's guesses, and rule out false positives, for low cost. The paper calls these side channels "Website Oracles".</p>

<p><img src="https://people.torproject.org/~mikeperry/images/WebsiteOracles-Table1.png" /></p>

<p>As this table illustrates, several of these Website Oracles are low-cost/low-effort and have high coverage. We're particularly concerned with DNS, <a href="https://en.wikipedia.org/wiki/Real-time_bidding" rel="nofollow">Real Time Bidding</a>, and <a href="https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol" rel="nofollow">OCSP</a>.</p>

<p>All of these oracles matter to varying degrees for non-Tor Internet users too, particularly in instances of centralized plaintext services. Because both DNS and OCSP are in plaintext, and because it is common practice for DNS to be centralized to public resolvers, and because OCSP queries are already centralized to the browser CAs, DNS and OCSP are good collection points to get website visit activity for large numbers of Internet users, not just Tor users.</p>

<p>Real Time Bidding ad networks are also a vector that Mozilla and EFF should be concerned about for non-Tor users, as they <a href="https://techpolicylab.uw.edu/wp-content/uploads/2018/07/ADINT-WPES17.pdf" rel="nofollow">leak even more information about non-Tor users</a> to ad network customers. Advertisers need not even pay anything or serve any ads to get information about all users who visit all sites that use the RTB ad network. On these bidding networks, visitor information is freely handed out to help ad buyers decide which users/visits they want to serve ads to. Nothing prevents advertisers from retaining this information for their own purposes, which also enables them to mount attacks, such as the one Tobias and Rasmus studied.</p>

<p>In terms of mitigating the use of these vectors in attacks against Tor, here's our recommendations for various groups in our community:</p>

<ul>
<li><b>Users: Do multiple things at once with your Tor client</b></li>
<p>Because Tor uses encrypted TLS connections to carry multiple circuits, an adversary that externally observes Tor client traffic to a Tor Guard node will have a significantly harder time performing classification if that Tor client is doing multiple things at the same time. This was studied in <a href="http://cacr.uwaterloo.ca/techreports/2015/cacr2015-09.pdf" rel="nofollow">section 6.3 of this paper</a> by Tao Wang and Ian Goldberg. A similar argument can be made for mixing your client traffic with your own Tor Relay or Tor Bridge that you run, but that is <a href="https://github.com/mikeperry-tor/vanguards/blob/master/README_SECURITY.md#the-best-way-to-run-tor-relays-or-bridges-with-your-service" rel="nofollow">very tricky to do correctly</a> for it to actually help.</p>
<li><b>Exit relay Operators: Run a local resolver; stay up to date with Tor releases</b></li>
<p>  Exit relay operators should follow our <a href="https://community.torproject.org/relay/setup/exit/#dns-on-exit-relays" rel="nofollow">recommendations for DNS</a>. Specificially: avoid public DNS resolvers like 1.1.1.1 and 8.8.8.8 as they can be easily monitored and have unknown/unverifiable log retention policies. This also means don't use public centralized DNS-Over-HTTPS resolvers, either (sadly). Additionally, we will be working on improvements to the DNS cache in Tor via <a href="https://trac.torproject.org/projects/tor/ticket/32678" rel="nofollow">ticket 32678</a>. When those improvements are implemented, DNS caching on your local resolver should be disabled, in favor of Tor's DNS cache.</p>
<li><b>Mozilla/EFF/AdBlocker makers: Investigate Real Time Bidding ad networks</b></li>
<p>The ability of customers of Real Time Bidding ad networks to <a href="https://techpolicylab.uw.edu/wp-content/uploads/2018/07/ADINT-WPES17.pdf" rel="nofollow">get so much information about website visit activity</a> of regular users without even paying to run ads should be a concern of all Internet users, not just Tor users. Some Real Time Bidding networks perform some data minimization and blinding, but it is not clear which ones do this, and to what degree. Any that perform insufficient data minimization should be shamed and added to bad actor block lists. For us, anything that informs all bidders that a visit is from Tor *before* they win the bid (e.g., by giving out distinct browser fingerprints that can be tied to Tor Browser or IP addresses that can be associated with exit relays) is leaking too much information.</p>
<p>The Tor Project would participate in an adblocker campaign that specifically targets bad actors such as cryptominers, fingerprinters, and Real Time Bidding ad networks that perform little or no data minimization to bidders. We will not deploy <a href="https://2019.www.torproject.org/projects/torbrowser/design/#philosophy" rel="nofollow">general purpose ad blocking</a>, though. Even for obvious ad networks that set visible cookies, <a href="https://cyberlaw.stanford.edu/files/publication/files/trackingsurvey12.pdf" rel="nofollow">coverage is 80% at best and often much lower</a>. We need to specifically target widely-used Real Time Bidding ad networks for this to be effective.</p>
<li><b>Website Operators: Use v3 Onion Services</b></li>
<p>If you run a sensitive website, hosting it as a v3 onion service is your best option. v2 onion services have their own Website Oracle that was mitigated by the v3 design. If you must also maintain a clear web presence, <a href="https://www.digitalocean.com/community/tutorials/how-to-configure-ocsp-stapling-on-apache-and-nginx" rel="nofollow">staple OCSP</a>, avoid Real Time Bidding ad networks, and avoid using large-scale CDNs with log retention policies that you do not directly control. For all services and third party content elements on your site, you should ensure there is no IP address retention, and no high-resolution timing information retention (log timestamps should be truncated at the minute, hour, or day; which level depends on your visitor frequency).</p>
<li><b>Researchers: Study Cover Traffic Defenses</b></li>
<p>We welcome and encourage research into cover traffic defenses for the general problem of Website Traffic Fingerprinting. We encourage researchers to review the <a href="https://github.com/torproject/tor/blob/master/doc/HACKING/CircuitPaddingDevelopment.md" rel="nofollow">circuit padding framework documentation</a> and use it to develop novel defenses that can be easily deployed in Tor.</p>
</ul>

    </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-286045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2019</p>
    </div>
    <a href="#comment-286045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286045" class="permalink" rel="bookmark">&gt; should truncated
*should…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; should truncated<br />
*should be truncated</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286094"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286094" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>GoodCatch (not verified)</span> said:</p>
      <p class="date-time">December 23, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286045" class="permalink" rel="bookmark">&gt; should truncated
*should…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-286094">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286094" class="permalink" rel="bookmark">I am guessing that what went…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am guessing that what went wrong here is that Mike Perry ran a spellchecker, but this software failed to notice a syntactical error (the original post inadvertently omitted a word which is needed for correct grammar, rather than containing the kind of spelling error which is easily caught by widely available spellcheckers).</p>
<p>I see a metaphor between the relationship</p>
<p>spellcheckers : reproducible builds and "public inspection" of code from torproject.org</p>
<p>to  </p>
<p>syntactical-errors : "upstream" provided software which is necessary for using Tor but which is not validated by reproducible builds and possibly not even by public inspection of open source code.</p>
<p>Unlike the minor error in Mike's post, a hard to spot flaw in a pseudo-random number generator could have devastating consequences for millions of endangered persons.</p>
<p>In other words, while reproducible builds is a huge advance, it may not protect us from subtle flaw introduced (and possibly even covertly mandated by the USG, not an organization to which it is easy for anyone anywhere to just say "no") into "upstream" code, much less hardware such as the USB controllers or hard disk controllers which are needed to operate the laptop on which I am writing this post.</p>
<p>Like it or not, we are all participants in an arms race: all the ordinary citizens versus all the governments and all the mega-corporations which are such overeager participants in "surveillance capitalism".</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286047"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286047" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Rahul Joon (not verified)</span> said:</p>
      <p class="date-time">December 20, 2019</p>
    </div>
    <a href="#comment-286047">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286047" class="permalink" rel="bookmark">To Developers,
all users…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To Developers,</p>
<p>all users using tor must have a bot which opens random sites, so bot makes it harder for government to find the request was sent by a human or bot</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286070"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286070" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286047" class="permalink" rel="bookmark">To Developers,
all users…</a> by <span>Rahul Joon (not verified)</span></p>
    <a href="#comment-286070">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286070" class="permalink" rel="bookmark">I guess it would be fairly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I guess it would be fairly easy to add some such capability to the start_tor_browser script.  The question is whether it would actually help keep users safer, or would just clog the Tor network without actually helping most users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286109"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286109" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>AroHaifo (not verified)</span> said:</p>
      <p class="date-time">December 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286047" class="permalink" rel="bookmark">To Developers,
all users…</a> by <span>Rahul Joon (not verified)</span></p>
    <a href="#comment-286109">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286109" class="permalink" rel="bookmark">Another idea the be awesome…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Another idea the be awesome with bot is to do it at random time that does just that in background. Make it act interactive on every website. Have a feature with internet bandwidth requirement for bot that make it look like your watching a video or downloading a file. Set them to close at random time or when there a lag as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286048"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286048" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2019</p>
    </div>
    <a href="#comment-286048">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286048" class="permalink" rel="bookmark">A new blog post by Mike…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A new blog post by Mike Perry *_*</p>
<p>thanks for making my day, we really missed your blog posts Mike</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286049"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286049" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bmuser1222 (not verified)</span> said:</p>
      <p class="date-time">December 20, 2019</p>
    </div>
    <a href="#comment-286049">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286049" class="permalink" rel="bookmark">Is bitmessage secure with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is bitmessage secure with tor with OnionTrafficOnly?<br />
How to create good noise traffic in tor with OnionTrafficOnly?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286049" class="permalink" rel="bookmark">Is bitmessage secure with…</a> by <span>bmuser1222 (not verified)</span></p>
    <a href="#comment-286067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286067" class="permalink" rel="bookmark">&gt; How to create good noise…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; How to create good noise traffic in tor with OnionTrafficOnly?</p>
<p>I also want to know this, explained in terms an ordinary Tor user who is not a coder can pursue.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286055" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2019</p>
    </div>
    <a href="#comment-286055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286055" class="permalink" rel="bookmark">avoid using large-scale CDNs…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>avoid using large-scale CDNs with log retention policies that you do not directly control</p></blockquote>
<p>Tor Browser should consider including Decentraleyes. And also just want to note that adblocker coverage (f.ex. with uBlock Origin) is better than it was in 2012.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286062"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286062" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2019</p>
    </div>
    <a href="#comment-286062">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286062" class="permalink" rel="bookmark">This post is devastatingly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This post is devastatingly sophisticated.  I love it.  More like this, please.  But not a plurality; the blog has to be approachable for general audiences.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286062" class="permalink" rel="bookmark">This post is devastatingly…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-286111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286111" class="permalink" rel="bookmark">I too loved the post and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I too loved the post and share everyone's gratitude for Mike Perry's work for TP, especially his role in developing Tor Browser, the Best-Thing-Yet for ordinary citizens.  I'd also love to see more posts covering technical issues (the state of crypto in Tor vis a via Quantum Cryptanalysis, for example), but agree that we need a mix of posts readable by prospective new users and prospective new Tor node operators, which ideally will inspire people considering trying Tor to follow through by adopting Tor for daily use.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286063"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286063" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286063">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286063" class="permalink" rel="bookmark">Lots of work has gone into…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Lots of work has gone into padding established connections, but what about an observer forcibly interrupting traffic to record timing or errors of disconnections? It occurred to me as I thought about how an external observer might be able to locate a Tor user who logs into ProtonMail, Tutanota, or a chat service and sends messages, for instance. I've lived in places where a momentary cut to the internet regularly correlated before hearing a police siren or helicopter.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286066" class="permalink" rel="bookmark">@ MP: it is very good to see…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ newcomers to this blog: Mike Perry led the coders who developed Tor Browser, which was probably the single most important product yet developed by Tor Project (after Tor itself of course).</p>
<p>@ MP: it is very good to see you are still hard at work keeping Tor safer!</p>
<p>&gt; Users: Do multiple things at once with your Tor client</p>
<p>Can you describe in more detail?  </p>
<p>Many users presumably use Tor circuits mostly for either</p>
<p>o browsing the internet with Tor Browser, reading pages, downloading/uploading files etc</p>
<p>o obtaining updates for their Debian system using the onion mirrors</p>
<p>I take it keeping multiple tabs open to various websites in Tor Browser is not enough?</p>
<p>I have tried to argue that OnionShare is potentially an easy, practical, and immediately available solution to the ever increasing problem of breaches of sensitive personal information including voter records, medical records, financial transactions, educational records, travel records, and so on and so forth.  Further, the threat landscape is ever expanding. In particular, multiple news stories over the past few years have confirmed in detail the warnings some posters made in comments to this blog over the past decade, that all ambitious national intelligence services increasingly have developed the means, motive, and opportunity for attacking large classes of ordinary citizens, seeking to mimic Keith Alexander's infamous injunction to US spies: collect everything about everyone.  Begin by targeting telecoms, government and mega-corporation contractors and leapfrog from there.  That is how NSA was doing in around 2005-2012, as verified in detail in the Snowden leaks. And to a great extent, whether they work for the government of FVEY nations, for Russia, for China, for Iran, all spooks think alike.</p>
<p>Because there is already a huge infrastructure devoted to collecting the "data exhaust" of every human living or dead, it seems reasonable to speculate that growing a sufficient privacy industry to enable ordinary citizens who use Tor Browser to also use OnionShare for many mandatory instances of sharing personally identifiable information, for the dual purposes of</p>
<p>o obstructing the spooks and behavioral advertisers/real-time-bidders who wish to spy on this sensitive information,</p>
<p>o diversifying the content of the content carried by their Tor circuits.</p>
<p>Any thoughts regarding this suggestion?</p>
<p>Could OnionShare possibly play a role in making name resolution harder for spooks and other baddies to exploit in order to attack ordinary citizens?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286069"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286069" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286069">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286069" class="permalink" rel="bookmark">&gt; Website Operators: Use v3…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Website Operators: Use v3 Onion Services</p>
<p>Ideally, there would be a single frequently updated HowTo for new (and experienced) node operators which explains all these things in detail, following the very simple and smart rule used by Tails Project in their documentation: </p>
<p>o explain the most essential points first, in the simplest possible terms</p>
<p>o congragulate the new operator on helping themselves and everyone in the world by running a Tor node.</p>
<p>o "Now, to make your node more resistant to various kinds of attacks on your users"</p>
<p>o detailed instructions in one place for the extra steps mentioned in the blog post above.</p>
<p>I believe that such a document could be useful both in helping potential new operators to understand quickly that they can absolutely get a node up and running with little effort, and in helping them to improve their node once basic maintainance has become second nature for them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286074"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286074" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286074">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286074" class="permalink" rel="bookmark">&gt; Users: Do multiple things…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Users: Do multiple things at once with your Tor client</p>
<p>I have a suggestion which I hope could greatly help both developers of open source software and also help diversify the content of Tor circuits at the user level:</p>
<p>Developers rely upon bug reports which are sometimes provided automatically (hopefully only if the user agrees).  I have long advocated making all such auto-reporting strongly anonymized and strongly encrypted.  Tails does this using Whisperback, but possibly OnionShare could also be used.  </p>
<p>A related example: Debian's venerable "popcon" should be strongly anonymized and strongly encrypted but it has never been either of those things.  Which means that privacy software takes a hit because Debian users concerned with privacy are hardly likely to enable popcon, which means that usage of privacy tools is severely under-counted.  (The Snowden leaks confirmed in details warnings that NSA and other bad actors routinely exploit bug reports and usage reports to target victims.  To state the obvious: knowing that a target (say the personal PC of a telecom engineer or government official) is running a no longer supported version of Windows 7 [looking at Putin] or that a targeted server is running a version of Azure which has an exploitable zero-day, is just what the cyberwarrior needs to start his or her attack.</p>
<p>Another example: the spell-checker in gedit could use some serious updating to include "novel" terms which posters are likely to use but which are too culturally recent to yet appear in standard word lists.  What is needed is automatic reporting to the spell-checker developer (if the user is willing to trust them) with words the current version didn't recognize.  Again, to state the obvious: this kind of information is invaluable to cyberspooks who want to use stylometry to  near-uniquely identify an "anonymous" writer, so is requires very strong protection.</p>
<p>What I propose is that TP try to develop a torified open-source free tool which is to automatic-bug/usage-reporting what Tor Browser is to web-browsing, which makes it easy for users to check developer web sites they are willing to trust with strongly encrypted and anonymized usage/error reports.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286254" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 05, 2020</p>
    </div>
    <a href="#comment-286254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286254" class="permalink" rel="bookmark">Most websites get some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Most websites get some contents from google.com on the fly, if I browse two of these websites at the same time and in different tab, do I share the same IP address with google?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286255"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286255" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 06, 2020</p>
    </div>
    <a href="#comment-286255">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286255" class="permalink" rel="bookmark">Tor Browser should block…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser should block recaptcha spyware. It is able to uniquely fingerprint most machines. Is extremely obfuscated with virtual machine but can unravel. In some days I am going to request audio captcha from each exit node which will cause recaptcha denial of service for each Tor user to raise awareness.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286259"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286259" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SHA1 WARNING (not verified)</span> said:</p>
      <p class="date-time">January 07, 2020</p>
    </div>
    <a href="#comment-286259">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286259" class="permalink" rel="bookmark">WARNING:  SHA1 is fully…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>WARNING:  SHA1 is fully broken!</p>
<p><a href="https://eprint.iacr.org/2020/014.pdf" rel="nofollow">https://eprint.iacr.org/2020/014.pdf</a><br />
<a href="https://sha-mbles.github.io/" rel="nofollow">https://sha-mbles.github.io/</a><br />
<a href="https://arstechnica.com/information-technology/2020/01/pgp-keys-software-security-and-much-more-threatened-by-new-sha1-exploit/" rel="nofollow">https://arstechnica.com/information-technology/2020/01/pgp-keys-softwar…</a><br />
"Behold: the world's first known chosen-prefix collision of widely used hash function.<br />
The new collision gives attackers more options and flexibility than were available with the previous technique. It makes it practical to create PGP encryption keys that, when digitally signed using SHA1 algorithm, impersonate a chosen target. More generally, it produces the same hash for two or more attacker-chosen inputs by appending data to each of them. The attack unveiled on Tuesday also costs as little as $45,000 to carry out."</p>
<p>Be prepared.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
