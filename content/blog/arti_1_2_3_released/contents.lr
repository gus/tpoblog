title: Security release: Arti 1.2.3.  (Please upgrade.)
---
author: nickm
---
pub_date: 2024-05-15
---
categories: announcements
---
summary:

Arti 1.2.3 is released and ready for download.
---
body:

We have released updates to Arti today,
to resolve a pair of security issues
related to circuit construction for onion services.

These vulnerabilities affect the crate `tor-circmgr 0.18.0`,
released along with Arti version 1.2.2.
They are fixed in `tor-circmgr 0.18.1`.
(Fixes will also appear in Arti version 1.2.4,
to be released on our regular schedule at the start of June.)

## Who is affected

If you use arti to connect to onion services,
or to run onion services,
and you are using Arti 1.2.2 or tor-circmgr 0.18.0,
you should upgrade.

(In Arti 1.2.1 and earlier, vanguards were still an experimental feature,
or absent, so those versions are classified as "not affected",
but downgrading to these versions will not improve your security.)

## Upgrade instructions

If you installed Arti via `cargo install`, use this command to update:
```
cargo install --locked --features=full arti
# or whatever --features you used before
```

If you obtained Arti as source code from git, fetch the tag `arti-v1.2.3`
and rebuild, with `cargo build --locked --release --features=full -p arti`.

## The issues

Both issues affect circuit construction when vanguards are enabled,
and affect the length.

First, when building anonymizing circuits to or from an onion service with
'lite' vanguards (the default) enabled,
the circuit manager code would build the circuits with one hop too few.
This makes users of this code more vulnerable to some kinds of traffic analysis
when they run or visit onion services.
This bug is tracked as issue [#1409], and as [TROVE]-2024-003.
Its severity is ["high"][SecurityPolicy].

Second, when 'full' vanguards are enabled, some circuits are supposed to be
built with an extra hop to minimize the linkability of the guard nodes.
In some circumstances,
the circuit manager would build circuits with one hop too few,
making it easier for an adversary to discover the L2 and L3 guards
of the affected clients and services.
This issue is tracked as issue [#1400], and as [TROVE]-2024-004.
Its severity is ["medium"][SecurityPolicy].

[#1400]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1400
[#1409]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1409
[TROVE]: https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/TROVE
[SecurityPolicy]: https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/SecurityPolicy
