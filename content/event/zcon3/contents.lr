title: Zcon3 (Las Vegas)
---
author: ggus
---
start_date: 2022-08-07
---
end_date: 2022-08-07
---
body:

Advancing Human Rights With Tor - Isabela Fernandes at Zcon3

YouTube: https://www.youtube.com/watch?v=5THwmYW4VMQ

The Tor Project is a non-profit that builds Tor, a technology that allows people to be anonymous online, protects their privacy and helps them bypass internet censorship. However, sometimes we think more about the technology and not as much about the people using it. There are many reasons why someone would use Tor, it can be simply because a father is worried about how much the 'internet' will learn about his kids. All the way to a campesino who needs protection while talking with lawyers about crimes committed by the local government. This talk will share some of these stories to help uplift the Tor Project mission, which is to help advance human rights.

Speaker: 

- Isabela Fernandes is the Executive Director of the Tor Project since November 2018. She joined the Tor Project as Project Manager in 2015, after working as Product Manager for International and Growth at Twitter for four years. Isabela has been part of the free software community since the late 90s, and in 2007 she co-founded and worked as Latin America Project Manager for North by South, a startup from San Francisco focused on free software projects. Isabela was also part of Brazil's Federal Government Free Software initiative, working in 2005 on the Ministry of Communications digital inclusion project and participating in 2006 on a project to migrate the IT of the Presidential Palace of Brazil to free software.
