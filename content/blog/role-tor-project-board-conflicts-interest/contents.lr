title: The Role of the Tor Project Board and Conflicts of Interest
---
author: isabela
---
pub_date: 2022-10-03
---
categories: ED
---
summary: Over the last couple of weeks, friends of the Tor Project have been raising questions about how Tor Project thinks of conflicts of interest and its board members, in light of the reporting from Motherboard about Team Cymru. I understand why folks would have questions, and so I want to write a bit about how the board of directors interacts with the Tor Project, and how our conflict of interest process works.
---
body:

Over the last couple of weeks, friends of the Tor Project have been raising questions about how Tor Project thinks of conflicts of interest and its board members, in light of the reporting from Motherboard about Team Cymru. I understand why folks would have questions, and so I want to write a bit about how the board of directors interacts with the Tor Project, and how our conflict of interest process works.

## The Role of the Board

First off, a word about non-profit boards of directors. Although every non-profit is [unique in its own way](https://en.wikipedia.org/wiki/Anna_Karenina_principle), the purpose of a board of an organization like The Tor Project, with a substantial staff and community, is not to set day-to-day policy or make engineering decisions for the organization. The board's primary role is a fiduciary one: to ensure that Tor is meeting its obligations under its bylaws and charter, and “hire/fire” power over the executive director. Although staff members may consult board members with relevant expertise over strategic decisions, and board members are selected in part for their background in the space, the board is separate from the maintenance and decision-making on Tor's code, and a board seat doesn't come with any special privileges over the Tor network. Board members may be consulted on technical decisions, but they don't make them. The Tor Project's staff and volunteers do. The Tor Project also has a [social contract](https://blog.torproject.org/tor-social-contract/) which everyone at Tor, including board members, has to comply with.

When we invite a person to join the Board, we are looking at the overall individual, their experience, expertise, character, and other qualities. We are not looking at them as representatives of another organization. But because Board members have fiduciary duties, they are are required to agree to a conflict of interest policy. [That policy](https://gitweb.torproject.org/company/policies.git/plain/corpdocs/Tor-Conflict-of-Interest-Policy.pdf) defines a conflict as “...the signee has an economic interest in, or acts as an officer or a director of, any outside entity whose financial interests would reasonably appear to be affected by the signee's relationship with The Tor Project, Inc. The signee should also disclose any personal, business, or volunteer affiliations that may give rise to a real or apparent conflict of interest.”

## Handling Conflicts of Interest

Like most conflict processes under United States law, non-profit conflicts rely on individuals to assess their own interests and the degree to which they might diverge. The onus is often on individual board members, who know the extent of their obligations, to raise questions about conflicts to the rest of the board, or to recuse themselves from decisions.

It also means that conflicts, and perceived conflicts, change over time. In the case of Rob Thomas's work with Team Cymru, the Tor Project staff and volunteers expressed concerns to me at the end of 2021, spurring internal conversations. I believe it is important to listen to the community, and so I worked to facilitate discussions and surface questions that we could try to address. During these conversations, it became clear that although Team Cymru may offer services that run counter to the mission of Tor, there was no indication that Rob Thomas's role in the provision of those services created any direct risk to Tor users, which was our primary concern. This was also discussed by the Board in March and the Board came to the same conclusion.

But of course, not actively endangering our users is a low bar. It is reasonable to raise questions about the inherent disconnection between the business model of Team Cymru and the mission of Tor which consists of private and anonymous internet access for all. Rob Thomas's reasons for choosing to resign from the board are his own, but it has become more clear over the months since our initial conversation how Team Cymru's work is at odds with the Tor Project's mission.

## What's Next

We at Tor, me, the board, staff and volunteers, will continue these conversations to identify how to do better from what we have learned here.

I have been working with the board to see where things can be done better in general. One of these initiatives is changing the Tor Project's board recruitment process. Historically, recruitment for board slots has been ad hoc - with current board members or project staff suggesting potential new candidates. This selection process has limited the pool of who has joined the board, and meant that we do not always reflect the diversity of experiences or perspectives of Tor users. For the first time we are running an open call for our board seats. Although this may seem unrelated to the idea of conflicts, we believe that more formalized processes create healthier boards that are able to work through potential conflict issues from a number of different angles.

Finally, let's talk about infrastructure for a moment. Our community has, rightly so, also raised concerns regarding the Tor Project usage of Team Cymru infrastructure. Team Cymru has donated hardware, and significant amounts of bandwidth to Tor over the years. These were mostly web mirrors and for internal projects like build and simulation machines.

Like all hardware that the Tor Project uses, we cannot guarantee perfect security when there is physical access, so we operate from a position of mistrust and rely on cryptographically verifiable reproducibility of our code to keep our users safe. As we would with machines hosted anywhere, the machines hosted at Cymru were cleanly installed using full disk encryption. This means that the set up with Team Cymru was not different from any other provider we would be using. So the level of risk for our users was the same when we used other providers.

But given the discussion of conflicts above, it's not tenable to continue to accept Team Cymru's donations of infrastructure. We have already been planning to move things out since [early 2022](https://gitlab.torproject.org/tpo/web/team/-/wikis/roadmap/2022). It is not a simple, or cheap task to move everything to some other location, so this process is going to take some time. We've already moved the web mirrors away, and are working on the next steps of this plan to completely move all services away from Team Cymru infrastructure. We thank the community for its patience with this process.
