title: Congestion Control Arrives in Tor 0.4.7-stable!
---
author: mikeperry
---
pub_date: 2022-05-04
---
categories:

relays
network
---
summary:

Tor has [released 0.4.7.7](https://forum.torproject.net/t/stable-release-0-4-7-7/3108), the
first stable Tor release with support for congestion control. Congestion
control will result in significant performance improvements in Tor, once Exit
relays upgrade. Relay operators: please read!

---
body:

Tor has [released 0.4.7.7](https://forum.torproject.net/t/stable-release-0-4-7-7/3108), the first stable Tor release with support for congestion control. Congestion control will eliminate the speed limit of current Tor, as well as reduce latency by minimizing queue lengths at relays. It will result in significant performance improvements in Tor, as well as increased utilization of our network capacity. In order for users to experience these benefits, we need Exit relay operators to upgrade as soon as possible. This post covers a bit of congestion control history, describes technical details, and contains important information for all relay and onion service operators.

## What is Congestion Control?

[Congestion
Control](https://en.wikipedia.org/wiki/Network_congestion#Congestion_control)
is an adaptive property of distributed networks, whereby a network and its
endpoints operate such that utilization is maximized, while minimizing a
constraint property, and ensuring fairness between connections. When this
optimization problem is solved, the optimal outcome is that all connections
transmit an equal fraction of the bandwidth of the slowest router in their
shared path, for every path through the network.

[TCP Congestion Control](https://en.wikipedia.org/wiki/TCP_congestion_control) solves this optimization problem primarily by minimizing packet drops as the constraint property, effectively increasing speed until router queues overflow, and reducing speed in proportion to these drops. In TCP terminology, the congestion control optimization problem is solved by setting the [Congestion Window](https://en.wikipedia.org/wiki/TCP_congestion_control#Congestion_window) equal to the [Bandwidth-Delay Product](https://en.wikipedia.org/wiki/Bandwidth-delay_product) of a path.

Some congestion control algorithms can make use of auxiliary information, such as latency, in order to anticipate congestion before the point at which queues overflow and packets drop. Notable examples are TCP Vegas, Bittorrent's LEDBAT, and Google's BBR.

## Congestion Control Means a Faster Tor

While Tor uses TCP between relays, Tor was designed without any end-to-end congestion control through the network itself. Instead, it set a fixed window size of 1000 512-byte Tor cells on a circuit. In the early days of Tor, this resulted in unbearable latency caused by excessive queue delay, because these windows were much larger than each client's fair share of the Bandwidth-Delay Product on any given circuit. In the early Tor days, users could wait for up to a minute for a page load to respond. This also meant that relays used a huge amount of memory in these cases.

Once [spare network capacity increased](https://metrics.torproject.org/bandwidth.html?start=2013-01-01&end=2016-06-01)
such that the spare Bandwidth-Delay Product of
circuits exceeded this fixed window size of 1000 cells, overall latency
improved due to lower queue delay, but throughput began to level off. Because
the Bandwidth-Delay Product was artificially limited to 1000 cells, this fixed
window size became a speed limit, with the property that lower-latency
circuits had higher throughput than high-latency circuits, directly in
proportion to their latency. 

This turning point with respect to the window size happened around 2015:

![Throughput and Latency from 2013-2016](onionperf-bw-latency-2013-2016.png)

When this [capacity turning point](https://metrics.torproject.org/bandwidth.html?start=2013-01-01&end=2016-06-01) was reached, congestion control became not only something that would improve latency, it would also significantly increase throughput.

This turning point made congestion control a top-priority improvement for the Tor network! Congestion control will remove this speed limit entirely, and will also reduce the impact of path latency on throughput.

## History of Congestion Control Research on Tor

Unfortunately, because Tor's circuit cryptography cannot support packet drops or reordering, the research community [struggled for nearly two decades](https://lists.torproject.org/pipermail/tor-dev/2020-January/014140.html) to determine a way to provide congestion control on the Tor network.

Crucially, we rejected mechanisms to provide congestion control by allowing packet drops, due to the [ability to introduce end-to-end side channels in the packet drop pattern](https://research.torproject.org/techreports/side-channel-analysis-2018-11-27.pdf).

This ultimately left only a very small class of candidate algorithms to consider: those that used Round-Trip Time to measure queue delay as a congestion signal, and those that directly measured Bandwidth-Delay Product.  The up-shot is that this class of algorithms only requires clients and Exit relays and onion services to upgrade; they do not require any changes to intermediate relays.

We ultimately specified three candidate algorithms informed by prior Tor and TCP research: Tor-Westwood, Tor-Vegas, and Tor-NOLA. These algorithms are detailed in [Tor Proposal 324](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/324-rtt-congestion-control.txt)

Tor-Westwood is based on the unnamed RTT threshold algorithm from the [DefenestraTor Paper](
https://www.cypherpunks.ca/~iang/pubs/defenestrator.pdf), in combination with Bandwidth-Delay Product estimation ideas from [TCP Westwood](https://en.wikipedia.org/wiki/TCP_Westwood).

Tor-Vegas is very closely based on [TCP Vegas](https://en.wikipedia.org/wiki/TCP_Vegas). TCP Vegas uses a much more fine-grained RTT ratio to directly estimate the total queue length on the path, and then targets a specific queue length as the constraint criteria. TCP Vegas is extremely efficient and effective, and is able to achieve fairness without any packet drops at all. However, it was never deployed on the Internet, because it was out-competed by the more aggressive and already deployed [TCP Reno](https://en.wikipedia.org/wiki/TCP_congestion_control#TCP_Tahoe_and_Reno). Because Reno continues increasing speed until packet drops happen, TCP Reno would end up soaking up the capacity of less aggressive Vegas flows that did not drop packets.

The final algorithm, Tor-NOLA, was created to test the behavior of Bandwidth-Delay Product estimation used directly as the congestion window, without any adaptation.

An additional component, called [Flow Control](https://en.wikipedia.org/wiki/Flow_control_%28data%29), is necessary to handle the case where an Internet destination or application is slower than Tor. We won't cover Flow Control in this post, but the interested reader can examine those details in [Section 4 of Proposal 324](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/324-rtt-congestion-control.txt#L621).

## Implementation, Simulation, and Deployment

We implemented all three algorithms (Tor-Westwood, Tor-Vegas, and Tor-NOLA) in Tor 0.4.7, and subjected them to [extensive evaluation](https://gitlab.torproject.org/mikeperry/tor/-/blob/cc_shadow_experiments_v2/SHADOW_EXPERIMENTS.txt) in the [Shadow Simulator](https://blog.torproject.org/new-foundations-tor-network-experimentation/).

The end result was that Tor-Westwood and Tor-NOLA exhibited [ack compression](http://softwareopal.com/qos/default.php?p=tcp-70), which caused them to wildly overestimate the Bandwidth-Delay Product, which lead to runaway congestion conditions. Standard mechanisms for dealing with ack compression, such as smoothing, probing, and long-term averaging did little to address this, perhaps because of the lack of packet drops as a backstop on queue pressure. Tor-Westwood also exhibited runaway conditions due to the nature of its RTT threshold. (As an aside, Google's [BBR algorithm](https://en.wikipedia.org/wiki/TCP_congestion_control#TCP_BBR) also has these problems, and relies on packet drops as a backstop as well).

Tor-Vegas performed beautifully, almost exactly as the theory predicted. Here's the Shadow Simulator's throughput graphs of clients with simulated locations in Germany and Hong Kong:

![Simulated HK and German Clients](Shadow-Throughput.png)

While there is still a difference in throughput between these two locations,
the speed limit from 0.4.6 Tor is clearly gone. End-to-end latency was not
affected at all, according to the simulator.

Additionally, Tor-Vegas was *not* out-competed by legacy Tor traffic, allowing
us to enable it as soon as 0.4.7 came out. We also gain protection from rogue
algorithms via the combination of [KIST](https://www.robgjansen.com/publications/kist-tops2018.pdf)
and [Circuit-EWMA](https://www.cypherpunks.ca/~iang/pubs/ewma-ccs.pdf), which were
previously deployed on Tor to address latency problems during the BDP
bottleneck era.

## Exit Relay Operators: Please Upgrade!

Users of Tor versions 0.4.7 and above will experience faster performance when using Exits or Onion Services that have upgraded to 0.4.7.

This means that in order for users to see the benefits of these improvements, we need our Exit relay operators to upgrade to the new Tor 0.4.7 stable series, asap!

Packages for [Debian](https://support.torproject.org/apt/tor-deb-repo/), [Ubuntu](https://support.torproject.org/relay-operators/operators-4/), and [Fedora/CentOS/RHEL](https://support.torproject.org/rpm/tor-rpm-install/) are already available. Please follow those links for instructions on using our packaging repos for those distributions, and upgrade asap!

BSD users should be able to install this release from their [flavor's ports system](https://lists.torproject.org/pipermail/tor-relays/2022-May/020528.html).

If you run into problems while upgrading your relay, you can ask your questions on the public [tor-relays](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays) mailing list and [Relay Operator](https://forum.torproject.net/c/support/relay-operator/17) sub-category on the Tor Forum. You can also get help by joining the channel [#tor-relays](https://support.torproject.org/get-in-touch/#irc-help).

## All Relay Operators: Be Prepared to Set Bandwidth Limits

Non-exit relay operators do not need to upgrade for congestion control to work, but this also means they may be surprised by the network effects of congestion control traffic running through their relays.

The faster performance and increased utilization of congestion control means that we will soon be able to use the full capacity of the Tor network. This means that all relays will soon experience new bottlenecks. Congestion control should prevent these bottlenecks from overwhelming relays completely, but this behavior may come as a surprise to operators who were used to the last several years of low CPU and bandwidth utilization.

We are already seeing an increase in the Advertised Bandwidth of relays as a
result of some higher-throughput congestion control circuit use, similar to
our previous flooding experiments, even though most clients are not yet using
congestion control:
 
![Advertised Bandwidth Increase](adv-bw-bump-scaled.png)

This increase is because Advertised Bandwidth is computed from the highest 7-day burst of traffic seen, where as Consumed Bandwidth is the average byte rate. As more clients upgrade, particularly after a Tor Browser Stable release with 0.4.7 is made, the Consumed Bandwidth of the network should also rise. We expect to make this Tor Browser Stable release on May 31st, 2022.

Once users migrate to this new release, relay operators who pay for bandwidth by the gigabyte may want to consider [enabling hibernation](https://support.torproject.org/relay-operators/limit-total-bandwidth/), to avoid surprise cost increases.

This increased traffic may also cause your relay CPU usage to spike, due to increased cryptographic load of the additional traffic. In theory, Tor-Vegas congestion control should treat CPU throughput bottlenecks exactly the same as bandwidth bottlenecks, and back off once CPU bottleneck causes queue delay. However, if you also pay for CPU, you may want to [rate limit your relay's bandwidth](https://support.torproject.org/relay-operators/bandwidth-shaping/).

Relays may also experience overload on the [Relay Search Portal](https://metrics.torproject.org/rs.html). Here is an example of that:

![Corona Overload](2022-overloaded_relay.jpg)

This overload indicator may appear for several reasons. If your relay has this overload indicator, follow the instructions on [our overload support page](https://support.torproject.org/relay-operators/relay-bridge-overloaded/), in order to diagnose the specific cause. If the cause is CPU overload, consider setting [bandwidth limits](https://support.torproject.org/relay-operators/bandwidth-shaping/), to reduce the traffic through your relay.

If you have issues diagnosing or eliminating the cause of overload, you can ask questions on the public [tor-relays](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays) mailing list and [Relay Operator](https://forum.torproject.net/c/support/relay-operator/17) sub-category on the Tor Forum. You can also get help by joining the channel [#tor-relays](https://support.torproject.org/get-in-touch/#irc-help).

## Onion Service Operators Should Also Upgrade

Just like Exit relays, Onion Services also need to upgrade to 0.4.7 for users to be able to use congestion control with them.

Additionally, Tor 0.4.7 has a security improvement for short-lived onion services, called [Vanguards-Lite](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/333-vanguards-lite.md). This system will reduce the risk of attacks that can discover the Guard relay of an onion service or onion client, so long as that onion service is around for a month or less. Longer lived onion services are still encouraged to use the [vanguards addon](https://github.com/mikeperry-tor/vanguards).

## Deployment Plan

The Tor Browser Alpha series already supports congestion control, but it won't experience improved performance unless an 0.4.7 Exit or Onion Service is used with it.

Because our network is roughly 25% utilized, we expect that throughput may be very high for the first few users who use 0.4.7 on fast circuits with fast 0.4.7 Exits, until the point where most clients have upgraded. At that point, a new equilibrium will be reached in terms of throughput and network utilization.

For this reason, we are holding back on releasing a Tor Browser Stable with congestion control, until enough Exits have upgraded to make the experience more uniform. We hope this will happen by May 31st.

Also for this reason, we won't be upgrading our [Tor performance metrics
sources](https://metrics.torproject.org/onionperf-throughput.html) to 0.4.7
until enough Exits have upgraded for those measurements to be an accurate
reflection of congestion control. So these improvements will not be reflected
in our performance metrics until we upgrade those onionperf instances, either.

## The Future

The astute reader will note that we rejected datagram transports. However, this does not mean that Tor will never carry UDP traffic. On the contrary, congestion control deployment means that queue delay and latency will be much more stable and predictable. This will enable us to [carry UDP without packet drops in the network](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/339-udp-over-tor.md), and only drop UDP at the edges, when the congestion window becomes full. We are hopeful that this new behavior will match what existing UDP protocols expect, allowing their use over Tor.

This still leaves the problem that very slow Tor relays may become a bottleneck, prohibiting the use of interactive voice and video over UDP while using them in a circuit. To address this problem, we will be examining our Guard and Fast relay bandwidth cutoffs, to avoid giving these flags to relays that are too slow to handle multiple clients at once.

Additionally, in Tor 0.4.8, we will be implementing a traffic splitting mechanism based on a previous Tor research paper called [Conflux](https://freehaven.net/anonbib/papers/pets2013/paper_65.pdf), with improvements from recent Multipath TCP research. This system is specified in [Tor Proposal 329](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/329-traffic-splitting.txt).

Conflux has the ability to rebalance traffic over multiple paths to an Exit relay, optimizing for either throughput, or latency.

With Conflux, Exit relays will become the new the speed limit of Tor, making fast Exits more valuable than ever before!
