title: Resistance, Change, and Freedom: Reflecting on 2022
---
author: isabela
---
pub_date: 2022-12-21
---
categories: 
fundraising
---
summary: Every end of year calls for reflection.
---
body:

Every end of year calls for reflection. In 2022, censorship and control of information has increased all around the world. I’m thinking about how we’ve seen setbacks in the world’s fight for human rights coming from all directions, like the wave of attacks on reproductive rights that has placed millions of people at risk, and how privacy and freedom online have been critical lifelines for many.

**In the midst of these setbacks, 2022 has also been a year of resistance. **In the Tor world, we’re currently resisting a [DDoS attack on the Tor network](https://forum.torproject.net/t/tor-relays-dos-attacks-status-update/5333). During the time we have been working hard to protect the network and mitigate the impact of this attack, organizations and supporters have come together to demonstrate their support for Tor. Our community has [raised awareness about the issue](https://bitcoinmagazine.com/culture/if-you-love-bitcoin-you-should-help-tor) and collectively contributed necessary funds to hire more developers for our network team. 

Support from our community will allow us to improve onion services and their defenses against attacks, and we plan to continue to improve the overall experience in deploying and maintaining onion sites in 2023. Increasing our support for onion services would not be possible without people who value Tor coming together in an act of solidarity.

Beyond improving the network and onion services, we look forward to continuing to support our relay operators community, which has been a goal for a long time. We are very happy that we now have the capacity to do so.

This year we’ve also resisted censorship against the Tor network and our website with the incredible help of our community. People came together to build a chain of resistance, from [sharing information on social media](https://twitter.com/0xggus/status/1574506323093381152) about Snowflake, to [helping others learn what to do to bypass government censorship](https://twitter.com/dw_persian/status/1577347148177444864), to donating their bandwidth to those censored by running a [proxy](https://snowflake.torproject.org/), to [legal support](https://twitter.com/torproject/status/1603328010983178240) in the face of censorship dictated by legislation.

![An image of a toot sharing a breakdown of the Snowflake proxies in November 17, 2022. In total, there were 128,408 proxies, with Germany contributing the most proxies.](snowflake-toot.png)

Resisting censorship has also driven the need for new strategies to reach users, like localized user support and new bridge and Tor Browser distribution mechanisms with our [new Telegram channels](https://t.me/TorProject) and improved GetTor support. Simultaneously we launched improvements to the Tor Browser’s user interface and censorship circumvention tools based on user research. 

Around the world, we’ve seen people using Tor in some of the most desperate situations resisting violations of their human rights, and we’ve resisted censorship and attacks in order to meet their needs. Resistance is hope, hope for change. 

**Fortunately some positive change happened this year, too.** I’m personally happy to see change coming to my home country, Brazil. We are finally moving away from a government that doesn't respect human rights to one that does. Sometimes change comes as society shifts its attitudes and makes the choice to stand up for their rights. I think we have to celebrate these changes, every victory, small or big, because they are part of a longer process. 

Changes are happening at the Tor Project that are worth celebrating, like looking critically at our code and understanding that we need to make a shift away from an older programming language and into a more modern implementation of Tor. I’m excited that we completed our first year of the work to rewrite Tor in Rust. In 2023, we anticipate releasing Arti 2.0.0, which is the next step in replacing the Tor C client with a more secure, easier to maintain Rust implementation. 

I also look to our partnership with [LEAP and Guardian Project](https://blog.torproject.org/tor-community-partners/). With these partners we’ve been working on a [Tor VPN client for Android](https://www.youtube.com/watch?v=uSyBZ7GIzJY) over the last year. This is quite a big change for Tor, and one we prioritized because of the needs of our community and for the future sustainability of the Tor Project. Next year, our goal is to release a minimum viable product for internal tests.

We, the Tor Project, and you, our community and our users, are all collaborating in a process of change by using, promoting, and volunteering with Tor and coming along with us as we evolve. I would like to celebrate all of us who share the same values of privacy and freedom online and who are dedicated to making positive changes for the internet and the world with these values.

![Staff and friends of Tor pose together in a large group outside on a cloudy day. People are making fun gestures, with some raising their hands towards the sky.](all-staff-ireland-2022.png "Tor staff in 2022")

As I reflect on the year and all the work that was made possible with collaboration, all the battles we fought as a community, and all of the resistance we supported by making Tor available to people who need it, **I think about the human rights and freedoms that are at stake.** 

At the Tor Project, our goal is to build technology to help people who are struggling to obtain or protect these rights. Your support makes this possible.** [If you value Tor, making a donation is one of the ways to ensure it's available for everyone who needs it](https://torproject.org/donate/donate-bp5-pbp)**.

Going into the new year, let’s celebrate our resistance and the changes we’ve made in the world because we stood up for our rights. And for the year that is arriving, let’s keep collaborating so freedoms and rights are a reality for everyone in this world. Happy 2023!