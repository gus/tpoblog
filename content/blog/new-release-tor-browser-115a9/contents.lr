title: New Alpha Release: Tor Browser 11.5a9 (Windows/macOS/Linux)
---
pub_date: 2022-04-26
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a9 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a9/).

Tor Browser 11.5a9 updates Firefox on Windows, macOS, and Linux to 91.8.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.4
- Tor Launcher 0.2.34
- Tor 0.4.7.5-alpha

The full changelog since [Tor Browser 11.5a8](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - Update Firefox to 91.8.0esr
  - Update NoScript to 11.4.4
  - Update Tor Launcher 0.2.34
  - Update Tor to 0.4.7.5-alpha
  - [Bug tor-browser#34366](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34366): The onion-location mechanism does not redirect to full URL
  - [Bug tor-browser-build#40469](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40469): Update zlib to 1.2.12 (CVE-2018-25032)
  - [Bug tor-browser#40773](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40773): Update the about:torconnect frontend page to match additional UI flows
  - [Bug tor-browser#40774](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40774): Update about:preferences page to match new UI designs
  - [Bug tor-browser#40822](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40822): Rebase tor-browser 11.5a9 to 91.8 Firefox
  - [Bug tor-browser#40862](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40862): Backport 1760674
