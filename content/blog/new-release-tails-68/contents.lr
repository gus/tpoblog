title: New Release: Tails 6.8
---
pub_date: 2024-10-08
---
author: tails
---
categories:

partners
releases
---
summary:

Tails 6.8 is out!
---
body:

## New features

### File system repair when unlocking the Persistent Storage

When the file system of the Persistent Storage has errors, Tails now offers
you to repair the file system when unlocking from the Welcome Screen.

Because not all file system errors can be safely recovered this way, we wrote
comprehensive documentation on how to recover data from the Persistent Storage
using complementary techniques.

## Changes and updates

  * Update _Tor Browser_ to [13.5.6](https://blog.torproject.org/new-release-tor-browser-1356).

  * Improve the notification when a network interface is disabled because MAC address anonymization failed.

## Fixed problems

  * Increase the maximum waiting time to 8 minutes when unlocking the Persistent Storage before returning an error. ([#20475](https://gitlab.tails.boum.org/tails/tails/-/issues/20475))

  * Hide shown password while unlocking Persistent Storage. ([#20498](https://gitlab.tails.boum.org/tails/tails/-/issues/20498))

  * Better handle failures in when sending _WhisperBack_ error messages:

    * Fix input fields when Tails is not connected to Tor. ([#19731](https://gitlab.tails.boum.org/tails/tails/-/issues/19731))
    * Fix accessibility of input fields with screen reader. ([#19903](https://gitlab.tails.boum.org/tails/tails/-/issues/19903))

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

## Get Tails 6.8

### To upgrade your Tails USB stick and keep your Persistent Storage

  * Automatic upgrades are available from Tails 6.0 or later to 6.8.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a manual upgrade.

### To install Tails 6.8 on a new USB stick

Follow our installation instructions:

  * Install from Windows

  * Install from macOS

  * Install from Linux

  * Install from Debian or Ubuntu using the command line and GnuPG

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

### To download only

If you don't need installation or upgrade instructions, you can download Tails
6.8 directly:

  * For USB sticks (USB image)

  * For DVDs and virtual machines (ISO image)

## Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.
