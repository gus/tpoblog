title: New Release: Tor Browser 13.5.10
---
pub_date: 2024-11-26
---
_discoverable: no
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 13.5.10 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.10 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.10/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

Ths release is part of the legacy channel, meant to extend the support for Windows 7/8/8.1 and macOS 10.12-10.14.
If your OS is not one of these, you should download the latest stable from the 14.0 series instead.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5.9](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Firefox to 115.18.0esr
  - Updated NoScript to 11.5.2
  - [Bug tor-browser#32668](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/32668): NoScript default whitelist re-appears on clicking NoScript Options / Reset
  - [Bug tor-browser#43257](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43257): NoScript-blocked content placeholders causing slow downs
  - [Bug tor-browser#43258](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43258): NoScript Lifecycle error on extension updates
  - [Bug tor-browser#43302](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43302): Rebase Tor Browser Legacy onto 115.18.0esr
- Build System
  - All Platforms
    - [Bug tor-browser-build#41286](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41286): Update the deploy update scripts to optinally take an override hash
    - [Bug tor-browser-build#41289](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41289): Fix single-browser in relprep.py
    - [Bug tor-browser-build#41290](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41290): Remove android signing in maint-13.5 branch
    - [Bug tor-browser-build#41300](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41300): Add bea, clairehurst, and jwilde to tb_builders
    - [Bug tor-browser-build#41303](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41303): Remove Android and Linux targets from browser-all and browser-all-desktop targets in release project
