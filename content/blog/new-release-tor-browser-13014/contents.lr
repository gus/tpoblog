title: New Release: Tor Browser 13.0.14
---
pub_date: 2024-04-16
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.14 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.14 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.14/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.13](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Tor to 0.4.8.11
  - [Bug tor-browser#41676](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41676): Set privacy.resistFingerprinting.testing.setTZtoUTC as a defense-in-depth
  - [Bug tor-browser#42335](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42335): Do not localize the order of locales for app lang
  - [Bug tor-browser#42428](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42428): Timezone offset leak via document.lastModified
  - [Bug tor-browser#42472](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42472): Timezone may leak from XSLT Date function
  - [Bug tor-browser#42508](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42508): Rebase Tor Browser stable onto 115.10.0esr
- Windows + macOS + Linux
  - Updated Firefox to 115.10.0esr
  - [Bug tor-browser#42172](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42172): browser.startup.homepage and TOR_DEFAULT_HOMEPAGE are ignored for the new window opened by New Identity
  - [Bug tor-browser#42236](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42236): Let users decide whether to load their home page on new identity.
  - [Bug tor-browser#42468](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42468): App languages not sorted correctly in stable
- Linux
  - [Bug tor-browser-build#41110](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41110): Avoid Fontconfig warning about "ambiguous path"
- Android
  - Updated GeckoView to 115.10.0esr
  - [Bug tor-browser#42509](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42509): Backport Android security fixes from Firefox 125
- Build System
  - All Platforms
    - Updated Go to 1.21.8
    - [Bug tor-browser-build#41107](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41107): Update download-unsigned-sha256sums-gpg-signatures-from-people-tpo for new type of URL
    - [Bug tor-browser-build#41122](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41122): Add release date to rbm.conf
  - Android
    - [Bug tor-browser-build#40992](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40992): Updated torbrowser_version number is not enough to change firefox-android versionCode number
