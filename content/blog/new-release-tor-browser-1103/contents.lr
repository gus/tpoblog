title: New Release: Tor Browser 11.0.3
---
pub_date: 2021-12-20
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.0.3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.3 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.3/)

This release updates Firefox to 91.4.1esr and picks up a number of bug
fixes. In particular, this release should fix various extension related
and crash issues Windows users were experiencing. Additionally, Linux
users especially on Ubuntu and Fedora systems were reporting fonts not
properly rendering, which should be solved by this release.

We used the opportunity to upgrade various components to their respective
latest versions as well: Tor to 0.4.6.9, OpenSSL to 1.1.1m, and snowflake for
enhanced censorship resistance.

## Full changelog

The full changelog since [Tor Browser 11.0.2](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - Update Firefox to 91.4.1esr
  - Update Tor to 0.4.6.9
  - Update OpenSSL to 1.1.1m
  - [Bug tor-browser-build#40393](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40393): Point to a forked version of pion/dtls with fingerprinting fix
  - [Bug tor-browser-build#40394](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40394): Bump version of Snowflake to 221f1c41
  - [Bug tor-browser#40646](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40646): Revert tor-browser#40475 and inherit upstream fix
  - [Bug tor-browser#40705](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40705): "visit our website" link on about:tbupdate pointing to different locations
  - [Bug tor-browser#40736](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40736): Disable third-party cookies in Private Browsing Mode
- Windows
  - [Bug tor-browser-build#40389](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40389): Remove workaround for HTTPS-Everywhere WASM breakage
  - [Bug tor-browser#40698](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40698): Addon menus missing content in TB11
  - [Bug tor-browser#40706](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40706): Fix issue in HTTPS-Everywhere WASM
  - [Bug tor-browser#40721](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40721): Tabs crashing on certain pages in TB11 on Win 10
  - [Bug tor-browser#40742](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40742): Remove workaround for fixing --disable-maintenance-service build bustage
- Linux
  - [Bug tor-browser-build#40387](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40387): Fonts of the GUI do not render after update
  - [Bug tor-browser#40685](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40685): Monospace font in browser chrome
- Build System
  - Windows + OS X + Linux
    - [Bug tor-browser-build#40403](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40403): Update Go to 1.16.12
  - OS X
    - [Bug tor-browser-build#40390](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40390): Remove workaround for macOS OpenSSL build breakage

Note: tor-browser#40698 and tor-browser#40721 were occuring due to the same underlying issue in Firefox. Mozilla has opted to make the ticket private, and we've followed suit on our end too.

## Known issues

Tor Browser 11.0.3 comes with a number of known issues (please check the following list before submitting a new bug report):

- [Bug tor-browser#40679](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679): Missing features on first-time launch in esr91 on MacOS
- [Bug tor-browser#40321](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40321): AV1 videos shows as corrupt files in Windows 8.1
- [Bug tor-browser#40693](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40693): Potential Wayland dependency
