title: Get the latest from Tor, join us for State of the Onion 2024
---
pub_date: 2024-11-07
---
author: pavel
---
categories:

community
fundraising

---
summary: We will be hosting our annual State of the Onion livestream, a virtual two-day event featuring the Tor Project's & Tor community's accomplishments of 2024. [Join us on November 13th and 20th at 17:00 UTC](https://blog.torproject.org/event/2024-state-of-the-onion/).
---
body:

Get ready for the 2024 recap of all things Tor: Our annual State of the Onion is less than a week away. It's our virtual two-day event where we share updates from our teams and the Tor community. As in previous years, we are organizing two streams one week apart. So please make sure to save the date for both days!

## Save the Dates

- Wednesday, November 13, 17:00 - 18:00 UTC – [Updates from the Tor Project](https://youtube.com/live/HjPdReNmf_g)
- Wednesday, November 20, 17:00 - 18:00 UTC – [Updates from the Tor Community](https://youtube.com/live/EODNtLqD7f8)

Both events will be live-streamed and available for replay on our [YouTube channel](https://www.youtube.com/@TheTorProject). Engage in the conversation on social media with the hashtag #StateOfTheOnion2024 or post questions and comments in the chat during the event.

----

## Tor Project Day - November 13, 2024

Join us on Day 1 as the Tor Project's teams discuss their work over the past year, from major updates to incremental improvements to exciting new tools for fighting censorship and increasing network resilience. Here are some highlights:

-   **Onion Services Team** - _Better Access to Onions_: Learn about tools like [Onionspray---a plug-and-play kit to "onionize" websites](https://blog.torproject.org/mediapart-launches-onion-service/)---and our ongoing work with the [Onion Service Ecosystem Portal](https://onionservices.torproject.org/) to make Onion Services more accessible to everyone.

-   **Anti-Censorship Team** - _Strengthening Tor's Foundation_: Find out more about how we're bolstering Tor's anti-censorship infrastructure, including what happened since launching [Webtunnel](https://blog.torproject.org/introducing-webtunnel-evading-censorship-by-hiding-in-plain-sight/), research on Snowflake, ensuring access to Tor with a new bridge distribution mechanism. 

-   **Network Health Team** - _Measuring Onions_: Get a download of what we're doing to make the user experience more consistent, faster, and safer by better distributing load across the network, according to relay capacity.

We'll also feature the Community Team's programs to promote Tor geographic relay diversity, the Application Team's learnings from over 15 years of developing Tor Browser, and much more.


## Tor Community Day - November 20, 2024

On Day 2, we turn the spotlight to Tor's community and the wider ecosystem with updates from projects that are built on Tor. Here's what to expect:

-   **Guardian Project** - _Mobilize Your Onions_: [The Guardian Project](https://guardianproject.info/apps/) will talk about features that make it easier for mobile devs to "onionize" their apps and services.

-   **Cwtch** - Learn more about [Cwtch](https://docs.cwtch.im/), a Tor-based secure communication tool establishing surveillance resistant channels between people and their journey to a stable release. They'll talk about new features like hybrid groups and bot integration.

-   And also featured for the first time,  **[Hushline](https://hushline.app/)** will showcase their encrypted  messaging capabilities and how it supports whistleblower and journalist protection.

-   **[Quiet](https://tryquiet.org/)** will join us again to present updates on encryption and user privacy, among other things.

-   As well as **[Ricochet-Refresh](https://www.ricochetrefresh.net/)** to talk about building private and secure p2p applications with Gosling.

-   **[OONI](https://ooni.org/)** will provide a  2024 in Review and look back at OONI's work measuring internet censorship worldwide, and their outlook for 2025.

See you there!

----
We couldn't do the work we're sharing at this year's State of the Onion without your support! This event is part of our [year-end fundraising campaign](https://blog.torproject.org/2024-fundraiser-donations-matched/). You can fund the Tor Project's work by [making a donation](https://donate.torproject.org/) today. 

Right now, if you make a donation to Tor, your donation will be matched by a generous donor. That means if you donate $25, they will also donate $25 — effectively doubling your gift and raising $50 for our teams.

[![Donate Button](donate-button.png "class=align-center")](https://torproject.org/donate/donate-bp2-yec2024)

You can check out our previous State of the Onion streams on our YouTube channel or replay some of our other virtual events we've hosted earlier this year like the [Postbox Launch](https://youtube.com/live/-K8ki7zBArs) and our event for [Global Encryption Day on Distributed Trust](https://www.youtube.com/live/r-wgsjyQlLs).
