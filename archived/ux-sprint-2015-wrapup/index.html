<!doctype html>
<html>
<head>
    <title>UX Sprint 2015 wrapup | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="UX Sprint 2015 wrapup | Tor Project">
    <meta property="og:description" content="Usability is critical to security. Additionally, there has been research on &#34;stopping points,&#34;...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/ux-sprint-2015-wrapup/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        UX Sprint 2015 wrapup
      </h1>
    <p class="meta">by dcf | February 9, 2015</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><a href="https://en.wikipedia.org/wiki/Usability" rel="nofollow">Usability</a> is critical to <a href="http://www.cc.gatech.edu/~keith/pubs/ieee-intro-usable-security.pdf" rel="nofollow">security</a>. Additionally, there has been research on "<a href="https://www.petsymposium.org/2012/papers/hotpets12-1-usability.pdf" rel="nofollow">stopping points</a>," common points where people would get frustrated with Tor enough that they would stop the installation process or stop using Tor. Usability issues also can degrade user experience, cause confusion, or even cause people to accidentally deanonymize themselves. </p>

<p>To address usability issues broadly, we brought together Tor developers, designers, users, and researchers to discuss usability problems and how to fix them. Last weekend, there was a <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2015UXsprint" rel="nofollow">user experience (UX) sprint</a> dedicated to improving the usability of Tor Browser.</p>

<p>A major part of the sprint was user testing the current <a href="https://blog.torproject.org/blog/tor-browser-403-released" rel="nofollow">Tor Browser (4.0.3)</a>. We asked users—some with prior Tor experience, some without—to perform some common tasks:</p>

<ul>
<li>Search for, download, and install Tor Browser</li>
<li>Do a web search</li>
<li>Watch a video</li>
<li>Use "New Identity"</li>
<li>Interact with and describe the current browser toolbar buttons</li>
</ul>

<p>The users performed this task in a "cognitive walkthrough" fashion, talking aloud while completing each of the tasks, explaining their understanding of the task and motivation for completing it in that specific way. With permission, we recorded the contents of the computer screen so developers could watch in another room. We hope to present the screen videos and other outcomes of the sprint at the upcoming <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2015WinterDevMeeting" rel="nofollow">winter dev meeting</a>.</p>

<p>Because of the limited size of the study (there were five participants), it's not possible to state with confidence what fraction of users will encounter major usability obstacles. However, it was effective at discovering and demonstrating issues that are likely to cause problems for many users. We will use our observations to guide future experiments. A few aspects stood out as deserving of further attention:</p>

<ul>
<li>
<p>While searching for Tor Browser, users encountered various sources for the browser other than our target <a href="https://www.torproject.org/download/download-easy.html" rel="nofollow">"Easy Download" page</a>, and also found some odd sources for documentation. Users who found a download page other than the "easy" version expressed varying degrees of confusion, depending on their landing page. Ticket <a href="https://trac.torproject.org/projects/tor/ticket/14686" rel="nofollow">#14685</a> is about finding a way to consolidate or drive users to our preferred download page.</p>
</li>
<li>
<p><a href="https://en.wikipedia.org/wiki/Gatekeeper_(OS_X)" rel="nofollow">Gatekeeper</a> on OS X makes it hard to install programs that aren't signed by an <a href="https://developer.apple.com/library/mac/documentation/Security/Conceptual/CodeSigningGuide/Introduction/Introduction.html" rel="nofollow">Apple certificate</a> or don't come from the App Store. All users were at least temporarily delayed by this dialog:</p>
<p><img src="https://extra.torproject.org/blog/2015-02-09-ux-sprint-2015-wrapup/gatekeeper-yosemite.png" alt="TorBrowser can't be opened because it is from an unidentified developer." /></p>
<p>Even though everyone was able to get past the dialog, it was a big obstacle to installation. (If it happens to you, the trick is to Ctrl-click on the Tor Browser icon and select "Open".) To solve this problem is not easy, but we can perhaps make it better by providing better documentation. Ticket <a href="https://trac.torproject.org/projects/tor/ticket/6540" rel="nofollow">#6540</a> tracks this issue.</p>
</li>
<li>
<p>If you try to run Tor Browser from a read-only filesystem, you get a misleading error message:</p>
<p><img src="https://extra.torproject.org/blog/2015-02-09-ux-sprint-2015-wrapup/firefox-is-already-open.png" alt="A copy of Firefox is already open. Only one copy of Firefox can be open at a time." /></p>
<p>This is a known issue, tracked in ticket <a href="https://trac.torproject.org/projects/tor/ticket/4782" rel="nofollow">#4782</a>, that affected some users. It happens on OS X if you run the Tor Browser app directly from the disk image (.dmg) instead of first copying it to the Applications folder.</p>
</li>
</ul>

<p>Tickets created as a result of the sprint have the <a href="https://trac.torproject.org/projects/tor/query?keywords=~uxsprint2015" rel="nofollow">#uxsprint2015</a> tag. In addition, the new <a href="https://trac.torproject.org/projects/tor/query?keywords=~tbb-usability-stoppoint&amp;status=!closed" rel="nofollow">#tbb-usability-stoppoint</a> tag marks stopping points, to help track usability issues like the above, which result in users being unable or unwilling to continue using the browser.</p>

<p>We hope that usability experiments and improvements will be ongoing. In the future, we'd like to test other aspects of Tor Browser, such as downloading files, updating, and managing Tor and non-Tor browsers, as well as expanding the tests to larger and diverse user groups. If you are interested in helping improve the usability of Tor Browser, <a href="https://www.torproject.org/about/contact" rel="nofollow">get in touch</a> by email or IRC. We need help from a lot of different kinds of people: designers, translators, programmers, and usability experts to name a few.</p>

<p>We thank the Tor Project for supporting the sprint, the participants, those who helped us recruit on short notice, and those who helped us plan and set goals and everyone who attended as a developer or observer: Arlo, Arthur, Ashkan, David, Griffin, Isis, Krishna, Linda, Mike, and Nima. We also thank the Tor help desk, whose <a href="https://trac.torproject.org/projects/tor/query?keywords=~tbb-helpdesk-frequent&amp;status=!closed" rel="nofollow">#tbb-helpdesk-frequent</a> tag helped us prioritize tickets. Special thanks go to Nima for suggesting the idea of a UX meeting in the first place.</p>

<p>Linda Lee and David Fifield</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/usability">
          usability
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-87380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87380" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2015</p>
    </div>
    <a href="#comment-87380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87380" class="permalink" rel="bookmark">I wonder if Alison&#039;s Library</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wonder if Alison's Library Freedom Project might provide some salient overlap here. Perhaps getting feedback from individuals/librarians in the trainings would provide answers to some of the questions or future questions?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-87525"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87525" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">February 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-87380" class="permalink" rel="bookmark">I wonder if Alison&#039;s Library</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-87525">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87525" class="permalink" rel="bookmark">Yup!!! I&#039;ve been doing just</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yup!!! I've been doing just that! Currently turning the feedback I've gathered into something usable, including a lot of suggestions to improve usability for basic users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-87574"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87574" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 12, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-87574">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87574" class="permalink" rel="bookmark">Fantastic!! Yay!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fantastic!! Yay!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-87407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 10, 2015</p>
    </div>
    <a href="#comment-87407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87407" class="permalink" rel="bookmark">i wonder can  isp   find out</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i wonder can  isp   find out i'm using tor?if "YES" how ? via the Ports are opened?Or something else</p>
<p>how can i bypass ISP ? using VPN before connecting to the toe is useful?</p>
<p>I think if ISP could Observationthe  tor users(i mean connecting  to the tor ) Is not only useful but also harmful .Especially in the countris that  tor users are few like syria</p>
<p>Imagine i leave a comment At an syria site .They recognize that the IP is of the  Tor network ... then  they ask The ISPs  to give them tor  users Statistics . Now the government is one step closer to me...</p>
<p>Sometimes  trying to stay hidden has An opposite result.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-87919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-87407" class="permalink" rel="bookmark">i wonder can  isp   find out</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-87919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87919" class="permalink" rel="bookmark">no, &quot;has An opposite result&quot;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no, "has An opposite result" is not true - in original case they immediately have your ip address and knock at your door.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-87998"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-87998" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2015</p>
    </div>
    <a href="#comment-87998">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-87998" class="permalink" rel="bookmark">Recently I discover the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Recently I discover the obfs4 option is in BridgeDB, it is a good news to Chinese people, and say thank you to Tor developers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-88535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 23, 2015</p>
    </div>
    <a href="#comment-88535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88535" class="permalink" rel="bookmark">Please make an usability</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please make an usability test before Vidalia would be erased from Tails.<br />
For all handy Vidalia(on Tails) features. 'torcc' editing,too!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89083" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2015</p>
    </div>
    <a href="#comment-89083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89083" class="permalink" rel="bookmark">Last two versions 4.0.3,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Last two versions 4.0.3, 4.0.4, downloading using wet (windows vista) had dns trouble. (tbb and tor not running) tbb and firefox have no dns trouble on the vista pc.</p>
<p>wget did dns resolution on xp, so something about this vista pc is not working for only wget. i'm posting in case somebody else is having this trouble.</p>
<p>above description was consistent for both the dist.torproject.org and <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a> paths.</p>
<p>----<br />
bookmark name problem.<br />
dragging links from web page creates bookmark without the page title. the title instead is the link's url. the same when dragging from icon in address bar.<br />
however dragging the tab creates bookmark with t page title as bookmark name.<br />
this occurs on the same vista pc (x64) and tbb 4.0.3, 4.0.4. i hadn't used tbb since a 3.5 version (whatever version was spring of 2014), which did not have this bookmark name problem.<br />
----<br />
my tbb ux complaints are really firefox ux complaints. i am among many users who dislike the inconveniences mozilla.org has created while making firefox almost as poorly usable as chrome browser.<br />
example: when esr bumps to next major version, esr will receive the newest change to searchplugins. compared to older searchplugins ux, the disadvantages outweigh advantages. major flaw is removal of search icon.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
