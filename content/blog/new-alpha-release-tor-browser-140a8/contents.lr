title: New Alpha Release: Tor Browser 14.0a8
---
pub_date: 2024-10-07
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 14.0a8 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 14.0a8 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/14.0a8/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5a11](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - [Bug 1607032 + 1918202#30543](https://gitlab.torproject.org/tpo/applications/1607032 + 1918202/-/issues/30543): compat: make spoofed orientation reflect spoofed screen dimensions [tor-browser]
  - [Bug tor-browser#42054](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42054): ESR128: investigate - thorin's list
  - [Bug tor-browser#42716](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42716): Disable unwanted about:* pages
  - [Bug tor-browser#43170](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43170): Disable user-agent spoofing in HTTP header
  - [Bug tor-browser#43173](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43173): Backport security fixes from Firefox 131
  - [Bug tor-browser#43178](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43178): Audit fingerprinting overrides (MozBug 1834274)
- Windows + macOS + Linux
  - Updated Firefox to 128.3.0esr
  - [Bug tor-browser#43098](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43098): YEC 2024 Takeover for Desktop Stable
  - [Bug tor-browser#43149](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43149): Update donate URL in YEC 2024 desktop
  - [Bug tor-browser#43164](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43164): Prevent search-bar from being auto-hidden when not used for awhile
  - [Bug tor-browser#43169](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43169): compat: align userAgent in navigator + HTTP Header
- Android
  - Updated GeckoView to 128.3.0esr
  - [Bug tor-browser#42660](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42660): Review the patch on Android's ProxySelector
  - Bug 43102: Android notifications tell to make Firefox your default browser
  - [Bug tor-browser#43151](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43151): MOZ_DATA_REPORTING, MOZ_TELEMETRY_REPORTING, MOZ_CRASHREPORTER, and MOZ_BACKGROUNDTASKS enabled on Android
- Build System
  - All Platforms
    - Updated Go to 1.23.2
    - [Bug tor-browser#43156](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43156): Update translation CI to account for the extended 13.5 release
    - [Bug tor-browser#43157](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43157): Move tb-dev to base-browser
    - [Bug tor-browser#43181](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43181): Run translation CI if there is a change in a string.xml file
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41247](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41247): Adapt tools/update-responses/update_responses to support multiple versions in the same xml files
    - [Bug tor-browser-build#41256](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41256): tools/signing/upload-update_responses-to-staticiforme should regenerate update-responses when it already exists
    - [Bug tor-browser-build#41259](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41259): Skip versions which don't set incremental_from when generating incrementals
